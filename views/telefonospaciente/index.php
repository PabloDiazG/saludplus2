<?php

use app\models\ModeloTelefonospaciente;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Lista de teléfonos de los pacientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-telefonospaciente-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Agregar teléfono de un paciente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'idPaciente',
                'label' => 'Nombre del Paciente',
                'value' => function ($model) {
                    $paciente = $model->idPaciente0;
                    return $paciente->nombre . ' ' . $paciente->apellido;
                }
            ],
            'telefonosPaciente',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ModeloTelefonospaciente $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]);
    ?>


</div>
