<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Pacientes;
use app\models\ModeloPacientes;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonospaciente $model */
$this->title = 'Actualizar Teléfono individual';
$this->params['breadcrumbs'][] = ['label' => 'Lista de teléfonos', 'url' => ['index']];
?>
<div class="modelo-telefonospaciente-update">

    <h1><?= Html::encode($this->title) ?></h1>

<?=
$this->render('_form', [
    'model' => $model,
    'pacientes' => $pacientes,
])
?>

</div>

