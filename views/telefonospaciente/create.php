<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonospaciente $model */
$this->title = 'Agregar nuevo teléfono';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Telefonospaciente', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-telefonospaciente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'pacientes' => $pacientes,
    ])
    ?>

</div>
