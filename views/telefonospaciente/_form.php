<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Pacientes;
use app\models\ModeloPacientes;
/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonospaciente $model */
/** @var array $pacientes Lista de pacientes */

?>

<div class="modelo-telefonospaciente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPaciente')->dropDownList(
        ArrayHelper::map($pacientes, 'id', 'nombreCompleto'), // Desplegable con nombres de pacientes
        ['prompt' => 'Seleccionar Paciente'] // Opción predeterminada
    ) ?>

    <?= $form->field($model, 'telefonosPaciente')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el teléfono del paciente']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
