<?php

use app\models\ModeloProporcionan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Lista de productos asignados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-proporcionan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Asignar nuevo producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'idProveedor',
                'label' => 'Proveedor',
                'value' => function ($model) {
                    return $model->idProveedor0 ? $model->idProveedor0->nombre : 'Desconocido';
                },
            ],
            [
                'attribute' => 'idProductos',
                'label' => 'Producto',
                'value' => function ($model) {
                    return $model->idProductos0 ? $model->idProductos0->nombre : 'Desconocido';
                },
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ModeloProporcionan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>

</div>
