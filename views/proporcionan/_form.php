<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ModeloProductos;
use app\models\ModeloProveedores;

/** @var yii\web\View $this */
/** @var app\models\ModeloProporcionan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-proporcionan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idProveedor')->dropDownList(
        ArrayHelper::map(ModeloProveedores::find()->all(), 'id', 'nombre'),
        ['prompt' => 'Seleccione un proveedor']
    ) ?>

    <?= $form->field($model, 'idProductos')->dropDownList(
        ArrayHelper::map(ModeloProductos::find()->all(), 'idProductos', 'nombre'),
        ['prompt' => 'Seleccione un producto']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
