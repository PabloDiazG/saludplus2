<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProporcionan $model */

$this->title = 'Asignar producto a un proveedor';
$this->params['breadcrumbs'][] = ['label' => 'Lista de productos asignados', 'url' => ['index']];

?>
<div class="modelo-proporcionan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
