<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\ModeloProductos;
use app\models\ModeloProveedores;

/** @var yii\web\View $this */
/** @var app\models\ModeloProporcionan $model */

$this->title = 'Producto asignado a un proveedor';
$this->params['breadcrumbs'][] = ['label' => 'Lista de productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="modelo-proporcionan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'idProveedor',
                'value' => function ($model) {
                    return $model->idProveedor0 ? $model->idProveedor0->nombre : 'Desconocido';
                },
            ],
            [
                'attribute' => 'idProductos',
                'value' => function ($model) {
                    return $model->idProductos0 ? $model->idProductos0->nombre : 'Desconocido';
                },
            ],
        ],
    ]) ?>

</div>
