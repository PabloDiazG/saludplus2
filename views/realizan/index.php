<?php

use app\models\ModeloRealizan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Registro de los procedimientos con su médico';
$this->params['breadcrumbs'][] = $this->title; 
?>  
<div class="modelo-realizan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Asignar procedimiento a un médico', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'idProcedimiento',
                'value' => function ($model) {
                    return $model->idProcedimiento0->numExpediente;
                },
                'label' => 'Número de Expediente',
            ],
            [
                'attribute' => 'idMedico',
                'value' => function ($model) {
                    return $model->idMedico0->nombreCompleto;
                },
                'label' => 'Médico',
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ModeloRealizan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>

</div>
