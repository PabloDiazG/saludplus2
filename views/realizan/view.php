<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ModeloRealizan $model */

$this->title = 'Listado de procedimiento individual';
$this->params['breadcrumbs'][] = ['label' => 'Listado de procedimientos asignados', 'url' => ['index']];

\yii\web\YiiAsset::register($this);
?>
<div class="modelo-realizan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'idProcedimiento',
                'value' => $model->idProcedimiento0->numExpediente,
                'label' => 'Número de Expediente',
            ],
            [
                'attribute' => 'idMedico',
                'value' => $model->idMedico0->nombreCompleto,
                'label' => 'Médico',
            ],
        ],
    ]) ?>

</div>
