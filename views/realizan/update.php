<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloRealizan $model */

$this->title = 'Procedimiento asignado individual';
$this->params['breadcrumbs'][] = ['label' => 'Listado de procedimientos asignados', 'url' => ['index']];

?>
<div class="modelo-realizan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
