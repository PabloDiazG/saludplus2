<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloRealizan $model */
/** @var yii\widgets\ActiveForm $form */
?>
<div class="modelo-realizan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'idProcedimiento')->dropDownList(
            \yii\helpers\ArrayHelper::map(app\models\ModeloProcedimientos::find()->all(), 'id', 'numExpediente'),
            ['prompt' => 'Seleccionar expediente']
    )
    ?>

    <?=
    $form->field($model, 'idMedico')->dropDownList(
            \yii\helpers\ArrayHelper::map(app\models\ModeloMedicos::find()->all(), 'id', 'nombreCompleto'),
            ['prompt' => 'Seleccionar médico']
    )
    ?>

    <div class="form-group">
<?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
