<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloPacientes $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-pacientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el nombre del paciente']) ?>

    <?= $form->field($model, 'apellido')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el apellido del paciente']) ?>
    
    <?= $form->field($model, 'dni')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el dni del paciente']) ?>

    <?=
    $form->field($model, 'idMedico')->dropDownList(
            \yii\helpers\ArrayHelper::map(\app\models\ModeloMedicos::find()->all(), 'id', 'nombreCompleto'),
            ['prompt' => 'Seleccione un médico']
    )
    ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese la dirección del paciente']) ?>

    <?= $form->field($model, 'expediente')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el expediente del paciente']) ?>

    <?= $form->field($model, 'atendido')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
