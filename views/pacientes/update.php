<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloPacientes $model */

$this->title = 'Actualizar paciente individual ';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Pacientes', 'url' => ['index']];

?>
<div class="modelo-pacientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
