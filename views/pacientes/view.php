<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ModeloPacientes $model */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="modelo-pacientes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'apellido',
            'dni',
            'direccion',
            'expediente',
            [
                'attribute' => 'idMedico',
                'value' => function ($model) {
                    return $model->idMedico0 ? $model->idMedico0->getNombreCompleto() : 'No asignado';
                },
                'label' => 'Nombre del Médico',
            ],
         
            'atendido:boolean',
        ],
    ]) ?>

</div>
