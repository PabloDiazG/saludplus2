<?php

use app\models\ModeloPacientes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Lista de pacientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-pacientes-index"> <!-- Contenedor principal de la vista -->

    <h1><?= Html::encode($this->title) ?></h1> <!-- Título principal de la página -->

    <p>
        <?= Html::a('Dar de alta un paciente', ['create'], ['class' => 'btn btn-success']) ?> <!-- Enlace para crear un nuevo paciente -->
    </p>

    <?=
    GridView::widget([// Widget GridView para mostrar datos tabulares
        'dataProvider' => $dataProvider, // Proveedor de datos
        'columns' => [// Columnas a mostrar
            // ['class' => 'yii\grid\SerialColumn'], // Columna de serie
            'nombre', // Columna de nombre
            'apellido', // Columna de apellido
            'dni', // Columna de DNI
            'direccion', // Columna de dirección
           
            
            //'expediente', // Columna de expediente
            //'idMedico', // Columna de ID de médico
            //'fechaDeConsulta', // Columna de fecha de consulta
            //'atendido', // Columna de estado de atención
            [
                'class' => ActionColumn::className(), // Columna de acciones
                'urlCreator' => function ($action, ModeloPacientes $model, $key, $index, $column) { // Función para crear URLs de acciones
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]);
    ?>


</div>
