<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models.ModeloEspecialidades $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $medicos */

?>

<div class="modelo-telefonosmedico-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idMedico')->dropDownList(
        \yii\helpers\ArrayHelper::map($medicos, 'id', 'nombre'), // Modificar 'nombre' según la columna que tenga el nombre del médico en tu tabla
        ['prompt' => 'Seleccione un médico']
    ) ?>

    <?= $form->field($model, 'telefonosMedico')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

