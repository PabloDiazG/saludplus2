<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloEspecialidades $model */
/** @var array $medicos */
/** @var array $especialidades */

$this->title = 'Asignar una especialidad a un médico';
$this->params['breadcrumbs'][] = ['label' => 'Listado de especialidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-especialidades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idMedico')->dropDownList($medicos, ['prompt' => 'Seleccione un médico']) ?>

    <?= $form->field($model, 'especialidades')->dropDownList(
        array_combine($especialidades, $especialidades),
        ['prompt' => 'Seleccione una especialidad']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
