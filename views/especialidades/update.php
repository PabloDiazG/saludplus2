<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ModeloEspecialidades;

/** @var yii\web\View $this */
/** @var app\models\ModeloEspecialidades $model */
/** @var array $medicos */
/** @var array $especialidades */

$this->title = 'Cambiar especialidad de ' . $model->medico->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => 'Lista de especialidades', 'url' => ['index']];

?>
<div class="modelo-especialidades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="modelo-especialidades-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'idMedico')->dropDownList(
            $medicos,
            ['prompt' => 'Seleccione un médico']
        ) ?>

        <?= $form->field($model, 'especialidades')->dropDownList(
            $especialidades,
            ['prompt' => 'Seleccione una especialidad']
        ) ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
