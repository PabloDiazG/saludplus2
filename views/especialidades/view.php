<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\ModeloMedicos;

/** @var yii\web\View $this */
/** @var app\models\ModeloEspecialidades $model */


$this->params['breadcrumbs'][] = ['label' => 'Listado de especialidades', 'url' => ['index']];

\yii\web\YiiAsset::register($this);

// Obtener el nombre del médico asociado
$nombreMedico = ModeloMedicos::findOne($model->idMedico)->nombreCompleto;

?>

<div class="modelo-especialidades-view">

    <h1><?= Html::encode($nombreMedico) ?></h1> <!-- Mostrar el nombre del médico en el título -->

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'idMedico',
                'value' => $nombreMedico, // Mostrar el nombre del médico en la tabla
                'label' => 'Nombre del Médico',
            ],
            'especialidades',
        ],
    ]) ?>

</div>
