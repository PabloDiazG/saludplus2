<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerCssFile(Url::to('@web/css/mejores-especialidades.css')); // Registro del archivo CSS para estilos específicos de "Mejores Especialidades"
$this->title = 'Mejores Especialidades'; // Título de la página
$this->params['breadcrumbs'][] = $this->title; // Migas de pan para la navegación

$this->registerCssFile('@web/css/estilos.css'); // Registro del archivo CSS para estilos generales
?>

<div class="contenedor-operaciones">
    <h1><?= Html::encode($this->title) ?></h1> <!-- Título principal de la página -->

    <ul class="lista-medicos">
        <?php foreach ($mejoresEspecialidades as $especialidad): ?> <!-- Iteración sobre las mejores especialidades -->
            <li><?= Html::encode($especialidad['especialidades']) ?> - <?= $especialidad['totalMedicos'] ?> médicos - <?= $especialidad['totalPacientes'] ?> pacientes</li> <!-- Elementos de la lista mostrando información sobre especialidades, médicos y pacientes -->
        <?php endforeach; ?>
    </ul>
</div>
