<?php

use app\models\ModeloEspecialidades;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Lista de especialidades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-especialidades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Especialidad', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'idMedico',
                'value' => function ($model) {
                    return $model->idMedico0 ? $model->idMedico0->nombreCompleto : 'No asignado';
                },
                'label' => 'Nombre del Médico',
            ],
            'especialidades',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ModeloEspecialidades $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>
</div>
