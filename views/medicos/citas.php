<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ModeloMedicos */
/* @var $modelPaciente app\models\ModeloPacientes */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Citas del Médico: ' . $model->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => 'Médicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="medicos-citas">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach ($model->pacientes as $paciente) : ?>
        <div class="etiqueta">
            <h3><?= Html::encode($model->nombre . ' ' . $model->apellidos) ?></h3>
            <div class="etiqueta-body">
                <p><?= Html::encode($paciente->nombre . ' ' . $paciente->apellido) ?></p>
                <p>Especialidad: <?= Html::encode($model->especialidades[0]->especialidades) ?></p>
            </div>
        </div>
    <?php endforeach; ?>

    <div class="form-add-paciente">
        <h3>Añadir Paciente a la Lista de Espera</h3>
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($modelPaciente, 'nombre')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelPaciente, 'apellido')->textInput(['maxlength' => true]) ?>

        <!-- Agrega aquí más campos del paciente según sea necesario -->

        <div class="form-group">
            <?= Html::submitButton('Añadir', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
