<?php
use yii\helpers\Html;
?>

<canvas id="pieChart" width="400" height="400"></canvas>

<?php
// Crea un array de colores para el gráfico
$colors = ['#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0', '#9966FF'];

// Prepara los datos para el gráfico
$data = [];
$labels = [];
$backgroundColors = [];

foreach ($detallesOperaciones as $detalle) {
    $labels[] = $detalle['nombre'] . ' ' . $detalle['apellidos'];
    $data[] = $detalle['totalOperaciones'];
    $backgroundColors = array_merge($backgroundColors, $colors);
}

// Convierte los datos a formato JSON
$labelsJson = json_encode($labels);
$dataJson = json_encode($data);
$backgroundColorsJson = json_encode($backgroundColors);

// Renderiza el script para inicializar el gráfico
$this->registerJs("
    var ctx = document.getElementById('pieChart').getContext('2d');
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: $labelsJson,
            datasets: [{
                data: $dataJson,
                backgroundColor: $backgroundColorsJson
            }]
        }
    });
");
?>
