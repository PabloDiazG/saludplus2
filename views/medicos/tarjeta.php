<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Tarjetas de Identificación de Médicos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid"> <!-- Contenedor fluido -->
    <h1><?= Html::encode($this->title) ?></h1> <!-- Título principal de la página -->

    <div class="cards-container"> <!-- Contenedor de tarjetas -->
        <?php
        echo ListView::widget([// Widget ListView para mostrar una lista de datos
            'dataProvider' => $dataProvider, // Proveedor de datos
            'itemView' => function ($model, $key, $index, $widget) { // Vista de cada elemento
                // Obtener las especialidades del médico
                $especialidades = app\models\ModeloEspecialidades::find() // Consulta para obtener especialidades
                        ->select('especialidades')
                        ->where(['idMedico' => $model->id]) 
                        ->column();

                // Si el médico no tiene especialidades, no se muestra
                if (empty($especialidades)) {
                    return '';
                }

                // Obtener el teléfono del médico
                $telefonoModel = app\models\ModeloTelefonosmedico::findOne(['idMedico' => $model->id]);
                $telefono = $telefonoModel ? $telefonoModel->telefonosMedico : 'Teléfono no disponible'; // Asignar teléfono o mensaje de no disponible
                // Definir la clase de color de la tarjeta según la primera especialidad encontrada
                $colorClass = 'default-color'; // Color por defecto
                if (!empty($especialidades)) { // Verificar si hay especialidades
                    // Tomar solo la primera especialidad
                    $primeraEspecialidad = $especialidades[0];
                    // Asignar el color correspondiente según la primera especialidad
                    switch ($primeraEspecialidad) {
                        case 'Cirugía General':
                            $colorClass = 'cirugia-color';
                            break;
                        case 'Neurología':
                            $colorClass = 'neurologia-color';
                            break;
                        case 'Oftalmología':
                            $colorClass = 'oftalmologia-color';
                            break;
                        case 'Cardiología':
                            $colorClass = 'cardiologia-color';
                            break;
                        case 'Dermatología':
                            $colorClass = 'dermatologia-color';
                            break;
                        case 'Ginecología y Obstet':
                            $colorClass = 'ginecologia-color';
                            break;
                        case 'Otorrinolaringología':
                            $colorClass = 'otorrinolaringologia-color';
                            break;
                        case 'Psiquiatría':
                            $colorClass = 'psiquiatria-color';
                            break;
                        case 'Medicina Interna':
                            $colorClass = 'medicina-interna-color';
                            break;
                        case 'Pediatría':
                            $colorClass = 'pediatria-color';
                            break;
                        default:
                            $colorClass = 'default-color';
                    }
                }

                return '
                    <div class="card mb-3 text-center ' . $colorClass . '" id="card_' . $index . '">
                        <div class="card-body">
                            <h5 class="card-title">' . Html::encode($model->nombreCompleto) . '</h5>
                            <p class="card-text">Especialidades: ' . Html::encode(implode(', ', $especialidades)) . '</p>
                            <p class="card-text">Teléfono: ' . Html::encode($telefono) . '</p>
                            <p class="card-text">Dirección: ' . Html::encode($model->direccion) . '</p>
                            <p class="card-text">Experiencia: ' . Html::encode($model->experiencia) . ' años</p>
                        </div>
                    </div>';
            },
        ]);
        ?>
    </div>
</div>

<style>
    ul.pagination {
        display: none !important;
    }

    div.summary {
        display: none !important;
    }

    /* Definir los estilos de los colores de las tarjetas */
    .cirugia-color {
        background-color: #FFD8D8; /* Cirugía General: tono de rojo claro */
    }

    .neurologia-color {
        background-color: #D8FFD8; /* Neurología: tono de verde claro */
    }

    .oftalmologia-color {
        background-color: #D8D8FF; /* Oftalmología: tono de azul claro */
    }

    .cardiologia-color {
        background-color: #B3E0E6; /* Cardiología: tono de amarillo claro */
    }

    .dermatologia-color {
        background-color: #FFD8FF; /* Dermatología: tono de rosa claro */
    }

    .ginecologia-color {
        background-color: #FFFFCC; /* Ginecología y Obstetricia: tono de amarillo claro */
    }

    .otorrinolaringologia-color {
        background-color: #E6CCFF; /* Otorrinolaringología: tono de morado claro */
    }

    .psiquiatria-color {
        background-color: #FFCCFF; /* Psiquiatría: tono de lila claro */
    }

    .medicina-interna-color {
        background-color: #FFCCFF; /* Psiquiatría: tono de lila claro */
    }

    .pediatria-color {
        background-color: #CCFFFF; /* Pediatría: tono de celeste claro */
    }

    .default-color {
        background-color: #aa8caf; /* Color por defecto: gris muy claro */
    }
</style>
