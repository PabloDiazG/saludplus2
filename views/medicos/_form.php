    <?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloMedicos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-medicos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el nombre del médico']) ?>
    
    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el apellido del médico']) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese la dirección del médico']) ?>

    <?= $form->field($model, 'experiencia')->textInput(['placeholder' => 'Ingrese su experiencia en años']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
