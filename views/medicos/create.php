<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloMedicos $model */

$this->title = 'Dar de alta un médico';
$this->params['breadcrumbs'][] = ['label' => 'Lista de médicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-medicos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
