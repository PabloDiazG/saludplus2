<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->registerCssFile(Url::to('@web/css/operaciones-exitosas.css'));
// Establecer el título de la página
$this->title = 'Operaciones Realizadas';
$this->params['breadcrumbs'][] = $this->title;

// Establecer los estilos CSS externos
$this->registerCssFile('@web/css/estilos.css');
?>

<!-- Estructura HTML -->
<div class="contenedor-operaciones"> <!-- Contenedor principal de las operaciones -->
    <h1><?= Html::encode($this->title) ?></h1> <!-- Título principal de la página -->

    <ul class="lista-medicos"> <!-- Lista de médicos -->
        <?php foreach ($detallesMedicos as $medico): ?> <!-- Bucle para cada médico -->
            <li><?= Html::encode($medico->nombre) ?> - <?= $medico->totalOperaciones ?> operaciones</li> <!-- Detalles del médico -->
        <?php endforeach; ?>
    </ul>
</div>
