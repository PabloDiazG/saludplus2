<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloMedicos $model */

$this->title = 'Actualizar médico individual';
$this->params['breadcrumbs'][] = ['label' => 'Lista de médicos', 'url' => ['index']];

?>
<div class="modelo-medicos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
