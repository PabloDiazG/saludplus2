<?php

use app\models\ModeloMedicos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Lista de médicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-medicos-index"> <!-- Contenedor principal de la vista -->

    <h1><?= Html::encode($this->title) ?></h1> <!-- Título principal de la página -->

    <p>
        <?= Html::a('Dar de alta un médico', ['create'], ['class' => 'btn btn-success']) ?> <!-- Enlace para crear un nuevo médico -->
    </p>


    <?=
    GridView::widget([// Widget GridView para mostrar datos tabulares
        'dataProvider' => $dataProvider, // Proveedor de datos
        'columns' => [// Columnas a mostrar
            //  ['class' => 'yii\grid\SerialColumn'], // Columna de serie
            'nombre', // Columna de nombre
            'direccion', // Columna de dirección
            'apellidos', // Columna de apellidos
            'experiencia', // Columna de experiencia
            [
                'class' => ActionColumn::className(), // Columna de acciones
                'urlCreator' => function ($action, ModeloMedicos $model, $key, $index, $column) { // Función para crear URLs de acciones
                    return Url::toRoute([$action, 'id' => $model->id]); // URL de la acción
                }
            ],
        ],
    ]);
    ?>


</div>