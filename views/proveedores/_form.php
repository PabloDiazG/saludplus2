<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloProveedores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-proveedores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cif')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el CIF del proveedor']) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el nombre del proveedor']) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese una descripción del proveedor']) ?>

    <?= $form->field($model, 'ubicacion')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese la ubicación del proveedor']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
