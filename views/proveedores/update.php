<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProveedores $model */

$this->title = 'Actualizar datos de proveedor individual';
$this->params['breadcrumbs'][] = ['label' => 'Lista de proveedores', 'url' => ['index']];

?>
<div class="modelo-proveedores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
