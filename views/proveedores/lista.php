<?php

use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Lista de Proveedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-lista"> <!-- Contenedor principal de la lista de proveedores -->

    <h1><?= Html::encode($this->title) ?></h1> <!-- Título principal de la página -->

    <div class="row"> <!-- Fila para mostrar los proveedores -->
        <?php foreach ($dataProvider->models as $model): ?> <!-- Bucle para iterar sobre los modelos de proveedores -->
            <div class="col-md-3"> <!-- Columna para cada proveedor -->
                <a href="#" data-toggle="modal" data-target="#modal-<?= $model['cif'] ?>" class="card-link"> <!-- Enlace para abrir el modal correspondiente a cada proveedor -->
                    <div class="card text-center mb-3"> <!-- Tarjeta para cada proveedor -->
                        <?php
                        // Definir la ruta de la imagen
                        $pngPath = Yii::getAlias('@web') . '/imagenes/proveedores/' . $model['cif'] . '.png';
                        $defaultImage = Yii::getAlias('@web') . '/imagenes/proveedores/default.png';
                        // Verificar si la imagen existe
                        $imagePath = file_exists(Yii::getAlias('@webroot') . '/imagenes/proveedores/' . $model['cif'] . '.png') ? $pngPath : $defaultImage;
                        ?>
                        <?=
                        Html::img(
                                $imagePath,
                                [
                                    'alt' => Html::encode($model['nombre_proveedor']),
                                    'class' => 'img-fluid card-img-top icon-sec', // Clase para la imagen
                                ]
                        );
                        ?>
                        <div class="card-body">
                            <h5 class="card-title"><?= Html::encode($model['nombre_proveedor']) ?></h5> <!-- Nombre del proveedor -->
                        </div>
                    </div>
                </a>
            </div>

            <?php
            Modal::begin([
                'id' => 'modal-' . $model['cif'], // ID del modal
                'title' => Html::encode($model['nombre_proveedor']), // Título del modal
                'size' => Modal::SIZE_LARGE, // Tamaño del modal
            ]);

            // Contenido del modal
            echo '<p><strong>CIF:</strong> ' . Html::encode($model['cif']) . '</p>';
            echo '<p><strong>Productos:</strong> ' . Html::encode($model['productos']) . '</p>';
            echo '<p><strong>Médicos:</strong> ' . Html::encode($model['medicos']) . '</p>';

            Modal::end(); // Fin del modal
            ?>
        <?php endforeach; ?>
    </div>
</div>