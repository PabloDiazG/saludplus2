<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProveedores $model */

$this->title = 'Crear proveedor';
$this->params['breadcrumbs'][] = ['label' => 'Lista de proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-proveedores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
