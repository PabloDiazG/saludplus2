<?php
/** @var yii\web\View $this */ // Declaración del tipo de vista

/** @var string $content */ // Declaración del contenido de la página
use app\assets\AppAsset; // Importación de la clase AppAsset
use app\widgets\Alert; // Importación del widget Alert
use yii\bootstrap4\Breadcrumbs; // Importación de la clase Breadcrumbs de Bootstrap 4
use yii\bootstrap4\Html; // Importación de la clase Html de Bootstrap 4
use yii\bootstrap4\Nav; // Importación de la clase Nav de Bootstrap 4
use yii\bootstrap4\NavBar; // Importación de la clase NavBar de Bootstrap 4
use yii\helpers\Url; // Importación de la clase Url de Yii

AppAsset::register($this); // Registro del asset de la aplicación
?>
<?php $this->beginPage() ?> <!-- Inicio de la página -->
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100"> <!-- Declaración del idioma y clase para altura completa -->
    <head>
        <meta charset="<?= Yii::$app->charset ?>"> <!-- Declaración de la codificación de caracteres -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> <!-- Declaración de la vista para dispositivos móviles -->
        <?php $this->registerCsrfMetaTags() ?> <!-- Registro de metaetiquetas CSRF -->
        <title><?= Html::encode($this->title) ?></title> <!-- Título de la página -->
        <?php $this->head() ?> <!-- Registro de assets -->
        <!-- Aquí se incluye el script de Chart.js -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?= Url::to('@web/logo.ico') ?>" /> <!-- Ícono para pestaña del navegador -->
        <link rel="icon" type="imagenes/x-icon" href="<?= Url::to('@web/logo.ico') ?>"/> <!-- Ícono para pestaña del navegador -->

    </head>
    <body class="d-flex flex-column h-100"> <!-- Clases para flexbox y altura completa -->
        <?php $this->beginBody() ?> <!-- Inicio del cuerpo de la página -->

        <header>
            <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name, // Etiqueta del logo
                'brandUrl' => Yii::$app->homeUrl, // URL del logo (página de inicio)
                'options' => [
                    'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top', // Clases para el navbar
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav mr-auto custom-margin-left'], // Opciones para el navbar
                'items' => [// Elementos del navbar
                    ['label' => 'Lista de Proveedores', 'url' => ['/proveedores/lista']], // Enlace a la lista de proveedores
                    ['label' => 'Tarjetas médicos', 'url' => ['/medicos/tarjeta']], // Enlace a las tarjetas médicas
                    ['label' => 'Mejores Especialidades', 'url' => ['/especialidades/mejores-especialidades']], // Enlace a las mejores especialidades
                    ['label' => 'Citas Médicas', 'url' => ['/citas/index']], // Enlace a las citas médicas
                    [
                        'label' => 'Registros', // Etiqueta del menú desplegable
                        'items' => [// Submenú
                            ['label' => 'Especialidades', 'url' => ['/especialidades/index']], // Enlace a la lista de especialidades
                            ['label' => 'Médicos', 'url' => ['/medicos/index']], // Enlace a la lista de médicos
                            ['label' => 'Pacientes', 'url' => ['/pacientes/index']], // Enlace a la lista de pacientes
                            ['label' => 'Procedimientos', 'url' => ['/procedimientos/index']], // Enlace a la lista de procedimientos
                            ['label' => 'Proveedores', 'url' => ['/proveedores/index']], // Enlace a la lista de proveedores
                            ['label' => 'Productos', 'url' => ['/productos/index']], // Enlace a la lista de productos
                            ['label' => 'Productos asignados a proveedores', 'url' => ['/proporcionan/index']], // Enlace a la lista de proporcionan
                            ['label' => 'Procedimientos asignados a médicos', 'url' => ['/realizan/index']], // Enlace a la lista de realizan
                            ['label' => 'Teléfonos de los médicos', 'url' => ['/telefonosmedico/index']], // Enlace a la lista de teléfonos de médicos
                            ['label' => 'Teléfonos de los pacientes', 'url' => ['/telefonospaciente/index']], // Enlace a la lista de teléfonos de pacientes
                            ['label' => 'Productos asignados a pacientes', 'url' => ['/utilizan/index']], // Enlace a la lista de utilizan
                        // ['label' => 'Contact', 'url' => ['/site/contact']],
                        ],
                    ],
                /*
                  Yii::$app->user->isGuest ? (
                  ['label' => 'Login', 'url' => ['/site/login']]
                  ) : (
                  '<li>'
                  . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                  . Html::submitButton(
                  'Logout (' . Yii::$app->user->identity->username . ')',
                  ['class' => 'btn btn-link logout']
                  )
                  . Html::endForm()
                  . '</li>'
                  )
                 */
                ],
            ]);
            NavBar::end();
            ?>
        </header>

        <main role="main" class="flex-shrink-0"> <!-- Rol del contenido principal -->
            <div class="container"> <!-- Contenedor principal -->
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], // Configuración de las migas de pan
                ])
                ?>
                <?= Alert::widget() ?> <!-- Widget para mostrar alertas -->
                <?= $content ?> <!-- Contenido de la página -->
            </div>
        </main>

        <footer class="footer mt-auto py-3 text-muted"> <!-- Pie de página -->
            <div class="container"> <!-- Contenedor del pie de página -->
                <p class="float-left">&copy; SaludPlus <?= date('Y') ?></p> <!-- Texto de derechos de autor -->
            </div>
        </footer>

        <?php $this->endBody() ?> <!-- Final del cuerpo de la página -->
    </body>
</html>
<?php $this->endPage() ?> <!-- Final de la página -->
