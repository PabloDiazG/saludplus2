<?php

use yii\helpers\Html;

/* @var $this yii\web\View */ // Declaración de la vista actual
/* @var $dataProvider yii\data\ActiveDataProvider */ // Declaración del proveedor de datos activos

$this->title = 'Lista de Médicos'; // Título de la página
$this->params['breadcrumbs'][] = $this->title; // Migas de pan para la navegación
?>
<div class="citas-index">
    <h1><?= Html::encode($this->title) ?></h1> <!-- Título principal de la página -->

    <div class="row">
        <?php foreach ($dataProvider->models as $model): ?> <!-- Iteración sobre los modelos de datos -->
            <div class="col-12 col-sm-6 col-md-4 mb-4"> <!-- Diseño responsivo de las tarjetas médicas -->
                <div class="card h-100"> <!-- Estilo de las tarjetas médicas -->
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title m-0">
                                <?= Html::encode($model->nombre) ?> <!-- Nombre del médico -->
                            </h5>
                            <button class="btn btn-primary btn-sm ver-medico-btn" data-toggle="collapse" data-target="#pacientes-<?= $model->id ?>" aria-expanded="false" aria-controls="pacientes-<?= $model->id ?>">
                                Ver Pacientes <!-- Botón para mostrar/ocultar pacientes -->
                            </button>
                        </div>
                        <ul id="pacientes-<?= $model->id ?>" class="collapse mt-2"> <!-- Lista de pacientes colapsable -->
                            <?php foreach ($model->pacientes as $index => $paciente): ?> <!-- Iteración sobre los pacientes -->
                                <li id="paciente-<?= $paciente->id ?>" style="display: flex; align-items: center; margin-bottom: 15px; <?= $index === 0 ? 'margin-top: 10px;' : '' ?>"> <!-- Estilo de cada paciente -->
                                    <?=
                                    Html::checkbox('atendido', $paciente->atendido, [
                                        'class' => 'form-check-input atendido-checkbox', // Checkbox para marcar si el paciente está atendido
                                        'data-id' => $paciente->id,
                                        'style' => 'margin-right: 10px;'
                                    ])
                                    ?>
                                    <span style="flex-grow: 1;"><?= Html::encode($paciente->nombre . ' ' . $paciente->apellido) ?></span> <!-- Nombre completo del paciente -->
                                    <?php if (!$paciente->atendido): ?> <!-- Si el paciente no está atendido -->
                                        <?=
                                        Html::a('Confirmar', ['marcar-atendido', 'id' => $paciente->id], [
                                            'class' => 'btn btn-success btn-sm confirmar-btn', // Botón para confirmar la atención
                                            'style' => 'margin-left: 5px; display: none;', // Estilo del botón (inicialmente oculto)
                                            'data' => [
                                                'confirm' => '¿Estás seguro de marcar como atendido?', // Confirmación antes de marcar como atendido
                                                'method' => 'post',
                                            ],
                                        ])
                                        ?>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<div id="paciente-details" style="display: none;">
    <!-- Aquí se mostrarán los detalles del paciente -->
</div>

<?php
$this->registerJs("
    $(document).on('change', '.atendido-checkbox', function() { // Función para mostrar/ocultar el botón de confirmación según el estado del checkbox
        var isChecked = $(this).prop('checked');
        var confirmarBtn = $(this).closest('li').find('.confirmar-btn');
        if (isChecked) {
            confirmarBtn.show(); // Mostrar el botón si el checkbox está marcado
        } else {
            confirmarBtn.hide(); // Ocultar el botón si el checkbox está desmarcado
        }
    });
");
?>
