<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProcedimientos $model */

$this->title = 'Crear nuevo procedimiento';
$this->params['breadcrumbs'][] = ['label' => 'Lista de Procedimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-procedimientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
