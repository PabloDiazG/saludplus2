<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ModeloProcedimientos $model */

$this->title = 'Procedimiento individual';
$this->params['breadcrumbs'][] = ['label' => 'Lista de Procedimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="modelo-procedimientos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de que quieres eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'numExpediente',
            'detalles',
            [
                'attribute' => 'idPaciente',
                'label' => 'Nombre del Paciente',
                'value' => function ($model) {
                    return $model->paciente ? $model->paciente->getNombreCompleto() : ''; // Modificado para mostrar el nombre del paciente
                },
            ],
        ],
    ]) ?>

</div>
