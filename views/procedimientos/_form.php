<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ModeloPacientes;

/** @var yii\web\View $this */
/** @var app\models\ModeloProcedimientos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-procedimientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numExpediente')->textInput(['placeholder' => 'Ingrese el número de expediente']) ?>

    <?= $form->field($model, 'detalles')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese los detalles del procedimiento']) ?>

    <?=
    $form->field($model, 'idPaciente')->dropDownList(
            ArrayHelper::map(ModeloPacientes::find()->all(), 'id', 'nombreCompleto'),
            ['prompt' => 'Seleccionar Paciente']
    )
    ?>

    <div class="form-group">
    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>