<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Lista de Procedimientos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-procedimientos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Procedimiento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'numExpediente',
            'detalles',
            [
                'attribute' => 'idPaciente',
                'label' => 'Nombre del Paciente',
                'value' => function ($model) {
                    return $model->paciente ? $model->paciente->getNombreCompleto() : ''; // Modificado para verificar si el paciente existe
                },
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index) { // Modificado para aceptar cualquier tipo de modelo
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]);
    ?>

</div>
