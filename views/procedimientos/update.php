<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProcedimientos $model */

$this->title = 'Procedimiento individual';
$this->params['breadcrumbs'][] = ['label' => 'Lista de procedimientos', 'url' => ['index']];

?>
<div class="modelo-procedimientos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
