<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloUtilizan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-utilizan-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPaciente')->dropDownList(
        \yii\helpers\ArrayHelper::map($pacientes, 'id', 'nombreCompleto'),
        ['prompt' => 'Seleccione el paciente']
    ) ?>

    <?= $form->field($model, 'idProductos')->dropDownList(
        \yii\helpers\ArrayHelper::map($productos, 'idProductos', 'nombre'),
        ['prompt' => 'Seleccione el producto']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

