<?php

use app\models\ModeloUtilizan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Lista de productos asignados a pacientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-utilizan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Asignar productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'idPaciente',
            'value' => function ($model) {
                return $model->idPaciente0->nombreCompleto; // Accede al nombre completo del paciente
            },
        ],
        [
            'attribute' => 'idProductos',
            'value' => function ($model) {
                return $model->idProductos0->nombre; // Accede al nombre del producto
            },
        ],
        [
            'class' => ActionColumn::className(),
            'urlCreator' => function ($action, $model, $key, $index) {
                return Url::toRoute([$action, 'id' => $model->id]);
            }
        ],
    ],
]); ?>


</div>
