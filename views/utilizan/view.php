<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Pacientes;
use app\models\Productos;

/** @var yii\web\View $this */
/** @var app\models\ModeloUtilizan $model */
$this->title = 'Producto asignado a paciente individual';
$this->params['breadcrumbs'][] = ['label' => 'Listado de productos asignados', 'url' => ['index']];

\yii\web\YiiAsset::register($this);
?>
<div class="modelo-utilizan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de que quieres eliminar este elemento?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'idPaciente',
                'value' => function ($model) {
                    return Pacientes::findOne($model->idPaciente)->nombreCompleto; // Suponiendo que tienes un método que devuelve el nombre completo del paciente en tu modelo Pacientes
                },
            ],
            [
                'attribute' => 'idProductos',
                'value' => function ($model) {
                    return Productos::findOne($model->idProductos)->nombre; // Suponiendo que tienes un atributo 'nombre' en tu modelo Productos
                },
            ],
        ],
    ])
    ?>

</div>
