<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloUtilizan $model */
$this->title = 'Productos asignados individualmente';
$this->params['breadcrumbs'][] = ['label' => 'Listado de productos asignados', 'url' => ['index']];

?>
<div class="modelo-utilizan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'pacientes' => $pacientes,
        'productos' => $productos,
    ])
    ?>

</div>
