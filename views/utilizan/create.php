<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloUtilizan $model */
$this->title = 'Asignar nuevo producto apaciente';
$this->params['breadcrumbs'][] = ['label' => 'Lista de productos asignados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-utilizan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'pacientes' => $pacientes,
        'productos' => $productos,
    ])
    ?>

</div>
