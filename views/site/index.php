<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var yii\web\View $this */
$this->title = 'Gestión de Centros de Salud';
$this->registerCssFile('@web/css/home.css');
?>

<div class="flex flex-col min-h-full"> <!-- Contenedor principal -->
    <main class="main"> <!-- Sección principal -->

        <section class="hero-section"> <!-- Sección del héroe -->
            <div class="hero-content"> <!-- Contenido del héroe -->
                <div class="hero-logo"> <!-- Logo del héroe -->
                    <img src="<?= Yii::getAlias('@web') ?>/imagenes/logoSolo.png" alt="Logo"> <!-- Imagen del logo -->
                </div>
                <h1 class="hero-title">Salud Plus</h1> <!-- Título del héroe -->
                <p>En nuestra misión por cuidar de tu bienestar, valoramos tanto tu salud como tu tiempo. Trabajamos eficientemente para disfrutar cada momento al máximo</p> <!-- Texto del héroe -->
            </div>
        </section>

        <section class="grid grid-2 grid-2-md"> <!-- Sección de la cuadrícula -->
            <div class="card"> <!-- Tarjeta 1 -->
                <h2 class="card-title">Operaciones Realizadas</h2> <!-- Título de la tarjeta -->
                <p class="card-text">Contamos con los mejores médicos del mundo. Descubre los que más resultados positivos han tenido. </p> <!-- Texto de la tarjeta -->
                <a href="<?= Url::to(['medicos/operaciones-exitosas']) ?>" class="card-link">Saber Más</a> <!-- Enlace de la tarjeta -->
            </div>
            <div class="card"> <!-- Tarjeta 2 -->
                <h2 class="card-title">Productos Más Proporcionados</h2> <!-- Título de la tarjeta -->
                <p class="card-text">Consulta de forma rápida los productos más proporcionados por los proveedores.</p> <!-- Texto de la tarjeta -->
                <a href="<?= Url::to(['productos/productos-proporcionados']) ?>" class="card-link">Saber Más</a> <!-- Enlace de la tarjeta -->
            </div>
        </section>

        <section class="secondary-section"> <!-- Sección secundaria -->
            <div class="secondary-card"> <!-- Tarjeta secundaria -->
                <h2 class="secondary-card-title">Asesoría Médica Profesional</h2> <!-- Título de la tarjeta secundaria -->
                <p class="secondary-card-text">Nuestros expertos médicos están disponibles para brindarte asesoramiento profesional y opciones de tratamiento adaptadas a tus necesidades.</p> <!-- Texto de la tarjeta secundaria -->
            </div>
        </section>

        <section class="why-section"> <!-- Sección de por qué elegirnos -->
            <h2 class="why-title">¿Por qué elegirnos?</h2> <!-- Título de por qué elegirnos -->
            <div class="why-grid why-grid-3 why-grid-3-md"> <!-- Cuadrícula de por qué elegirnos -->

                <div class="why-card"> <!-- Tarjeta de por qué elegirnos -->
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"> <!-- Icono -->
                    <circle cx="12" cy="12" r="10"></circle>
                    <path d="M15 9l-6 6"></path>
                    <path d="M9 9h6v6H9z"></path>
                    </svg>
                    <h3 class="why-card-title">Atención de Calidad</h3> <!-- Título de la tarjeta -->
                    <p class="why-card-text">Nuestra institución ofrece una atención médica de calidad, proporcionando un entorno propicio para el ejercicio profesional y la excelencia en el cuidado del paciente.</p> <!-- Texto de la tarjeta -->
                </div>
                <div class="why-card"> <!-- Tarjeta de por qué elegirnos -->
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"> <!-- Icono -->
                    <circle cx="12" cy="12" r="10"></circle>
                    <path d="M15 9l-6 6"></path>
                    <path d="M9 9h6v6H9z"></path>
                    </svg>
                    <h3 class="why-card-title">Doctores con Experiencia</h3> <!-- Título de la tarjeta -->
                    <p class="why-card-text">Únete a nuestro equipo de médicos altamente experimentados, donde podrás aplicar tus conocimientos y habilidades para brindar un cuidado de calidad a nuestros pacientes.</p> <!-- Texto de la tarjeta -->
                    <a href="<?= Url::to(['medicos/operaciones-modal']) ?>" id="openModalLink" class="card-link" onclick="
                           $('#myModal').modal('show');
                           $.ajax({
                               url: $(this).attr('href'),
                               type: 'GET',
                               success: function (response) {
                                   $('#modalBody').html(response);
                               },
                               error: function (xhr, status, error) {
                                   console.error(xhr.responseText);
                               }
                           });
                           return false; // Evita que el enlace se comporte como un enlace normal
                       ">Saber Más</a>

                </div>
                <div class="why-card"> <!-- Tarjeta de por qué elegirnos -->
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"> <!-- Icono -->
                    <circle cx="12" cy="12" r="10"></circle>
                    <path d="M15 9l-6 6"></path>
                    <path d="M9 9h6v6H9z"></path>
                    </svg>
                    <h3 class="why-card-title">Tecnología Avanzada</h3> <!-- Título de la tarjeta -->
                    <p class="why-card-text">Contamos con equipos y tecnologías de última generación, lo que te permite ofrecer tratamientos innovadores y efectivos a tus pacientes, garantizando resultados óptimos en su atención médica.</p> <!-- Texto de la tarjeta -->
                </div>
            </div>
        </section>
    </main>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Gráfico de operaciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modalBody">
                <!-- Aquí se cargarán los detalles de las operaciones exitosas -->
            </div>
        </div>
    </div>
</div>

