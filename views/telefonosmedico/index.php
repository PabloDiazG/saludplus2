<?php

use app\models\ModeloTelefonosmedico;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Lista de teléfonos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-telefonosmedico-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Agregar nuevo teléfono a un médico', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //   ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'idMedico',
                'value' => function ($model) {
                    return $model->idMedico0->nombreCompleto; // Accediendo al nombre completo del médico
                },
                'label' => 'Médico', // Etiqueta personalizada para el nombre del médico
            ],
            'telefonosMedico',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ModeloTelefonosmedico $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
