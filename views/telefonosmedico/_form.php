<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Medicos;
use app\models\ModeloMedicos;
/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonosmedico $model */
/** @var array $medicos Lista de médicos */

?>

<div class="modelo-telefonosmedico-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idMedico')->dropDownList(
        ArrayHelper::map($medicos, 'id', 'nombreCompleto'), // Desplegable con nombres de médicos
        ['prompt' => 'Seleccionar Médico'] // Opción predeterminada
    ) ?>

    <?= $form->field($model, 'telefonosMedico')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el teléfono del médico']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
