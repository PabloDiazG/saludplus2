<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonosmedico $model */
$this->title = 'Agregar nuevo teléfono';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Telefonosmedicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-telefonosmedico-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'medicos' => $medicos,
    ])
    ?>

</div>
