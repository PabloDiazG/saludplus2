<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Medicos;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonosmedico $model */

$this->title = 'Teléfono individual';
$this->params['breadcrumbs'][] = ['label' => 'Lista de teléfonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="modelo-telefonosmedico-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de que quieres eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'idMedico',
                'value' => function ($model) {
                    return $model->idMedico0->nombreCompleto;
                },
                'label' => 'Médico', // Etiqueta personalizada para el nombre del médico
            ],
            'telefonosMedico',
        ],
    ]) ?>

</div>
