<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Medicos; 
use app\models\ModeloMedicos;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonosmedico $model */

$this->title = 'Actualizar Teléfono individual';
$this->params['breadcrumbs'][] = ['label' => 'Lista de teléfonos', 'url' => ['index']];

?>
<div class="modelo-telefonosmedico-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'medicos' => $medicos, 
    ]) ?>

</div>

