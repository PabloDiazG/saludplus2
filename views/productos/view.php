<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ModeloProductos $model */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="modelo-productos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idProductos' => $model->idProductos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'idProductos' => $model->idProductos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de que quieres eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'precio',
            'area',
            [
                'attribute' => 'limitada',
                'value' => function ($model) {
                    return $model->limitada == 1 ? 'Sí' : 'No';
                }
            ],
            'stock',
        ],
    ]) ?>

</div>
