<?php

use app\models\ModeloProductos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Lista de Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-productos-index"> <!-- Contenedor principal de la vista -->

    <h1><?= Html::encode($this->title) ?></h1> <!-- Título principal de la página -->

    <p>
        <?= Html::a('Crear Producto', ['create'], ['class' => 'btn btn-success']) ?> <!-- Enlace para crear un nuevo producto -->
    </p>

    <?=
    GridView::widget([// Widget GridView para mostrar datos tabulares
        'dataProvider' => $dataProvider, // Proveedor de datos
        'columns' => [// Columnas a mostrar
            //   ['class' => 'yii\grid\SerialColumn'], // Columna de serie
            'nombre', // Columna de nombre
            'precio', // Columna de precio
            'area', // Columna de área
            [
                'attribute' => 'limitada', // Columna de limitada
                'value' => function ($model) { // Función para obtener el valor formateado
                    return $model->limitada == 1 ? 'Sí' : 'No'; // Retorna 'Sí' si el valor es 1, de lo contrario, 'No'
                }
            ],
            'stock', // Columna de stock
            [
                'class' => ActionColumn::className(), // Columna de acciones
                'urlCreator' => function ($action, ModeloProductos $model, $key, $index, $column) { // Función para crear URLs de acciones
                    return Url::toRoute([$action, 'idProductos' => $model->idProductos]);
                }
            ],
        ],
    ]);
    ?>


</div>
