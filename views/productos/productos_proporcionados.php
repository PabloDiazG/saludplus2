<?php
use yii\helpers\Html;

use yii\helpers\Url;

$this->title = 'Productos más proporcionados por los proveedores';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('@web/css/operaciones-exitosas.css'); // Cambiado a operaciones-exitosas.css
?>

<div class="contenedor-operaciones" style="margin-top: 60px;">
    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="lista-medicos">
        <?php foreach ($productosProporcionados as $producto): ?>
            <li><?= Html::encode($producto['nombre']) ?> - <?= Html::encode($producto['cantidad_proveedores']) ?> proveedores</li>
        <?php endforeach; ?>
    </ul>
</div>
    