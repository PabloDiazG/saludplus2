<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProductos $model */

$this->title = 'Producto individual';
$this->params['breadcrumbs'][] = ['label' => 'Lista de Productos', 'url' => ['index']];

?>
<div class="modelo-productos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
