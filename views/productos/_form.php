<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloProductos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el nombre del producto']) ?>

    <?= $form->field($model, 'precio')->textInput(['placeholder' => 'Ingrese el precio del producto']) ?>

    <?= $form->field($model, 'area')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\ModeloProductos::find()->select(['area'])->distinct()->all(),
            'area',
            'area'
        ),
        ['prompt' => 'Seleccionar área']
    ) ?>

    <?= $form->field($model, 'stock')->textInput(['placeholder' => 'Ingrese la cantidad de stock disponible']) ?>
    
    <?= $form->field($model, 'limitada')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>