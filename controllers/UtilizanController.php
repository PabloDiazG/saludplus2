<?php

namespace app\controllers;

use app\models\ModeloUtilizan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UtilizanController implements the CRUD actions for ModeloUtilizan model.
 */
class UtilizanController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    // Se define un filtro de verbos HTTP para restringir las acciones a verbos específicos
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all ModeloUtilizan models.
     *
     * @return string
     */
    public function actionIndex() {
        // Se crea un proveedor de datos activo para recuperar todos los modelos de ModeloUtilizan
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloUtilizan::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        // Se renderiza la vista 'index' con los datos proporcionados por el proveedor de datos
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloUtilizan model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        // Se renderiza la vista 'view' con el modelo correspondiente al ID proporcionado
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModeloUtilizan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new ModeloUtilizan();

        // Obtener nombres completos de pacientes y productos
        $pacientes = \app\models\Pacientes::find()->all();
        $productos = \app\models\Productos::find()->all();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
                    'pacientes' => $pacientes,
                    'productos' => $productos,
        ]);
    }

    /**
     * Updates an existing ModeloUtilizan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
    // Se encuentra el modelo correspondiente al ID proporcionado
    $model = $this->findModel($id);

    // Obtener nombres completos de pacientes y productos
    $pacientes = \app\models\Pacientes::find()->all();
    $productos = \app\models\Productos::find()->all();

    // Si se envía un formulario POST, se intenta cargar y guardar el modelo actualizado
    if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
    }

    // Se renderiza la vista 'update' con el modelo para mostrar el formulario de actualización
    return $this->render('update', [
        'model' => $model,
        'pacientes' => $pacientes, 
        'productos' => $productos,
    ]);
}

    /**
     * Deletes an existing ModeloUtilizan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        // Se encuentra y elimina el modelo correspondiente al ID proporcionado
        $this->findModel($id)->delete();

        // Se redirige a la página 'index'
        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloUtilizan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ModeloUtilizan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        // Se busca el modelo ModeloUtilizan por su ID
        if (($model = ModeloUtilizan::findOne(['id' => $id])) !== null) {
            return $model;
        }

        // Si el modelo no se encuentra, se lanza una excepción de error 404
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
