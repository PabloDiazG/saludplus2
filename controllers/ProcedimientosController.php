<?php

namespace app\controllers;

use app\models\ModeloProcedimientos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProcedimientosController implements the CRUD actions for ModeloProcedimientos model.
 */
class ProcedimientosController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        // Agrega el filtro de verbos HTTP a los comportamientos heredados
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all ModeloProcedimientos models.
     *
     * @return string
     */
     public function actionIndex()
    {
        // Consulta con la relación para obtener los nombres de los pacientes
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloProcedimientos::find()->with('paciente'),
        ]);

        // Renderiza la vista 'index' con los datos proporcionados
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloProcedimientos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        // Renderiza la vista 'view' para mostrar los detalles de un procedimiento específico
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModeloProcedimientos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        // Crea un nuevo modelo de procedimiento
        $model = new ModeloProcedimientos();

        // Si se envían datos de formulario y se guarda el modelo exitosamente
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                // Redirige a la vista 'view' para mostrar los detalles del nuevo procedimiento
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            // Carga los valores predeterminados del modelo
            $model->loadDefaultValues();
        }

        // Renderiza la vista 'create' para mostrar el formulario de creación de procedimientos
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing ModeloProcedimientos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        // Busca el modelo de procedimiento por su ID
        $model = $this->findModel($id);

        // Si se envían datos de formulario y se actualiza el modelo exitosamente
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // Redirige a la vista 'view' para mostrar los detalles del procedimiento actualizado
            return $this->redirect(['view', 'id' => $model->id]);
        }

        // Renderiza la vista 'update' para mostrar el formulario de actualización de procedimientos
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ModeloProcedimientos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        // Busca y elimina el modelo de procedimiento por su ID
        $this->findModel($id)->delete();

        // Redirige a la vista 'index' para mostrar todos los procedimientos restantes
        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloProcedimientos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ModeloProcedimientos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        // Busca y devuelve el modelo de procedimiento por su ID
        if (($model = ModeloProcedimientos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        // Si el modelo no se encuentra, lanza una excepción de página no encontrada
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
