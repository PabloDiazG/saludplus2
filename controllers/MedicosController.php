<?php

namespace app\controllers;

use app\models\ModeloMedicos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModeloRealizan;

/**
 * MedicosController implements the CRUD actions for ModeloMedicos model.
 */
class MedicosController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        // Hereda los comportamientos de la clase padre y agrega el filtro de verbos HTTP
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all ModeloMedicos models.
     *
     * @return string
     */
    public function actionIndex() {
        // Crea un proveedor de datos para mostrar todos los médicos
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloMedicos::find(),
        ]);

        // Renderiza la vista 'index' con los datos proporcionados
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloMedicos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        // Renderiza la vista 'view' para mostrar los detalles de un médico específico
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModeloMedicos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        // Crea un nuevo modelo de médico
        $model = new ModeloMedicos();

        // Si se envían datos de formulario y se guarda el modelo exitosamente
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                // Redirige a la vista 'view' para mostrar los detalles del nuevo médico
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            // Carga los valores predeterminados del modelo
            $model->loadDefaultValues();
        }

        // Renderiza la vista 'create' para mostrar el formulario de creación de médicos
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing ModeloMedicos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        // Busca el modelo de médico por su ID
        $model = $this->findModel($id);

        // Si se envían datos de formulario y se actualiza el modelo exitosamente
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // Redirige a la vista 'view' para mostrar los detalles del médico actualizado
            return $this->redirect(['view', 'id' => $model->id]);
        }

        // Renderiza la vista 'update' para mostrar el formulario de actualización de médicos
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ModeloMedicos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        // Busca y elimina el modelo de médico por su ID
        $this->findModel($id)->delete();

        // Redirige a la vista 'index' para mostrar todos los médicos restantes
        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloMedicos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ModeloMedicos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        // Busca y devuelve el modelo de médico por su ID
        if (($model = ModeloMedicos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        // Si el modelo no se encuentra, lanza una excepción de página no encontrada
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Action to display the top doctors with the most successful operations.
     *
     * @return string
     */
    public function actionOperacionesExitosas() {
        // Consulta los 5 médicos con más operaciones exitosas
        $medicosConOperaciones = ModeloRealizan::find()
                ->select(['idMedico', 'COUNT(idProcedimiento) as totalOperaciones'])
                ->groupBy('idMedico')
                ->orderBy(['totalOperaciones' => SORT_DESC])
                ->limit(5) // Limita la consulta a los 5 médicos con más operaciones
                ->asArray()
                ->all();

        // Obtiene los detalles de los médicos
        $detallesMedicos = [];
        foreach ($medicosConOperaciones as $medico) {
            $detallesMedico = ModeloMedicos::findOne($medico['idMedico']);
            if ($detallesMedico) {
                $detallesMedico['totalOperaciones'] = $medico['totalOperaciones'];
                $detallesMedicos[] = $detallesMedico;
            }
        }

        // Renderiza la vista con los datos obtenidos
        return $this->render('operaciones-exitosas', [
                    'detallesMedicos' => $detallesMedicos,
        ]);
    }

    /**
     * Lists all appointments for doctors.
     *
     * @return string
     */
    public function actionCitas($id) {
        // Encuentra la instancia de ModeloMedicos
        $model = ModeloMedicos::findOne($id);

        // Encuentra la instancia de ModeloPacientes
        $modelPaciente = new ModeloPacientes(); // Esto es un ejemplo, asegúrate de crear el modelo adecuadamente
        // Renderiza la vista y pasa las variables necesarias

        return $this->render('citas', [
                    'model' => $model,
                    'modelPaciente' => $modelPaciente,
        ]);
    }

    public function actionOperacionesModal() {
        // Consulta los detalles de las operaciones exitosas
        $detallesOperaciones = ModeloMedicos::find()
                ->select([
                    'medicos.nombre',
                    'medicos.apellidos',
                    'COUNT(procedimientos.id) AS totalOperaciones' // Contamos el número de procedimientos realizados
                ])
                ->leftJoin('realizan', 'medicos.id = realizan.idMedico') // Unimos la tabla de realizan con medicos
                ->leftJoin('procedimientos', 'realizan.idProcedimiento = procedimientos.id') // Unimos la tabla de procedimientos
                ->groupBy(['medicos.id', 'medicos.nombre', 'medicos.apellidos']) // Agrupamos por médico
                ->orderBy(['totalOperaciones' => SORT_DESC]) // Ordenamos por número de operaciones descendente
                ->limit(5) // Limitamos a los 5 médicos con más operaciones
                ->asArray()
                ->all();

        // Renderiza la vista 'operaciones-exitosas' con los detalles obtenidos
        return $this->render('operaciones-exitosas-modal', [
                    'detallesOperaciones' => $detallesOperaciones,
        ]);
    }

    /**
     * Action to display a card with information about all doctors.
     *
     * @return string
     */
    public function actionTarjeta() {
        // Crea un proveedor de datos para mostrar todos los médicos sin paginación
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloMedicos::find(),
            'pagination' => false, // Desactiva la paginación
        ]);

        // Renderiza la vista 'tarjeta' con los datos proporcionados
        return $this->render('tarjeta', [
                    'dataProvider' => $dataProvider,
        ]);
    }

}
