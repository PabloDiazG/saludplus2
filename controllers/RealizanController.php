<?php

namespace app\controllers;

use app\models\ModeloRealizan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RealizanController implements the CRUD actions for ModeloRealizan model.
 */
class RealizanController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all ModeloRealizan models.
     *
     * @return string
     */
    public function actionIndex() {
        // Crea un proveedor de datos para mostrar todos los registros de realizaciones
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloRealizan::find(),
        ]);

        // Renderiza la vista 'index' con los datos proporcionados
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloRealizan model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        // Renderiza la vista 'view' para mostrar los detalles de una realización específica
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModeloRealizan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        // Crea un nuevo modelo de realización
        $model = new ModeloRealizan();

        // Si se envían datos de formulario y se guarda el modelo exitosamente
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                // Redirige a la vista 'view' para mostrar los detalles de la nueva realización
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            // Carga los valores predeterminados del modelo
            $model->loadDefaultValues();
        }

        // Renderiza la vista 'create' para mostrar el formulario de creación de realización
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing ModeloRealizan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        // Busca el modelo de realización por su ID
        $model = $this->findModel($id);

        // Si se envían datos de formulario y se actualiza el modelo exitosamente
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // Redirige a la vista 'view' para mostrar los detalles de la realización actualizada
            return $this->redirect(['view', 'id' => $model->id]);
        }

        // Renderiza la vista 'update' para mostrar el formulario de actualización de realización
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ModeloRealizan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        // Busca y elimina el registro de realización por su ID
        $this->findModel($id)->delete();

        // Redirige a la vista 'index' para mostrar todos los registros de realizaciones restantes
        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloRealizan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ModeloRealizan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        // Busca y devuelve el modelo de realización por su ID
        if (($model = ModeloRealizan::findOne(['id' => $id])) !== null) {
            return $model;
        }

        // Si el modelo no se encuentra, lanza una excepción de página no encontrada
        throw new NotFoundHttpException('La página solicitada no existe.');
    }

}
