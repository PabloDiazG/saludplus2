<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Citas;
use app\models\ModeloPacientes;
use app\models\ModeloMedicos;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CitasController extends Controller {

    // Acción para mostrar todas las citas
    public function actionIndex() {
        // Se crea un proveedor de datos para mostrar las citas
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Citas::find(),
            'pagination' => false,
        ]);

        // Se renderiza la vista 'index' con los datos proporcionados
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    // Acción para marcar una cita como atendida
    public function actionMarcarAtendido($id) {
        // Busca al paciente por su ID
        $paciente = ModeloPacientes::findOne($id);
        
        // Si el paciente existe
        if ($paciente !== null) {
            // Cambia el valor de la propiedad 'atendido' a 1 (true)
            $paciente->atendido = 1;
            // Guarda los cambios en la base de datos
            $paciente->save();
        }

        // Redirige de vuelta a la página anterior
        return $this->redirect(Yii::$app->request->referrer);
    }

    // Método protegido para encontrar un modelo de cita por su ID
    protected function findModel($id) {
        // Busca y devuelve el modelo de cita por su ID
        if (($model = Citas::findOne($id)) !== null) {
            return $model;
        }
        // Si el modelo no se encuentra, lanza una excepción de página no encontrada
        throw new NotFoundHttpException('La página solicitada no existe.');
    }

}
