<?php

namespace app\controllers;

use Yii;
use app\models\ModeloProveedores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProveedoresController implements the CRUD actions for ModeloProveedores model.
 */
class ProveedoresController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all ModeloProveedores models.
     *
     * @return string
     */
    public function actionIndex() {
        // Crea un proveedor de datos para mostrar todos los registros de proveedores
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloProveedores::find(),
        ]);

        // Renderiza la vista 'index' con los datos proporcionados
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloProveedores model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        // Renderiza la vista 'view' para mostrar los detalles de un registro de proveedor específico
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModeloProveedores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        // Crea un nuevo modelo de proveedor
        $model = new ModeloProveedores();

        // Si se envían datos de formulario y se guarda el modelo exitosamente
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                // Redirige a la vista 'view' para mostrar los detalles del nuevo registro de proveedor
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            // Carga los valores predeterminados del modelo
            $model->loadDefaultValues();
        }

        // Renderiza la vista 'create' para mostrar el formulario de creación de proveedor
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing ModeloProveedores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        // Busca el modelo de proveedor por su ID
        $model = $this->findModel($id);

        // Si se envían datos de formulario y se actualiza el modelo exitosamente
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // Redirige a la vista 'view' para mostrar los detalles del registro de proveedor actualizado
            return $this->redirect(['view', 'id' => $model->id]);
        }

        // Renderiza la vista 'update' para mostrar el formulario de actualización de proveedor
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ModeloProveedores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        // Busca y elimina el registro de proveedor por su ID
        $this->findModel($id)->delete();

        // Redirige a la vista 'index' para mostrar todos los registros de proveedores restantes
        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloProveedores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ModeloProveedores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        // Busca y devuelve el modelo de proveedor por su ID
        if (($model = ModeloProveedores::findOne(['id' => $id])) !== null) {
            return $model;
        }

        // Si el modelo no se encuentra, lanza una excepción de página no encontrada
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /* Acción Lista */

    public function actionLista() {
        // Consulta SQL para obtener la lista de proveedores con sus productos y médicos asociados
        $consultaSql = "
        SELECT pr.nombre AS nombre_proveedor, 
               pr.cif,
               GROUP_CONCAT(DISTINCT pd.nombre ORDER BY pd.nombre ASC SEPARATOR ', ') AS productos,
               GROUP_CONCAT(DISTINCT m.nombre ORDER BY m.nombre ASC SEPARATOR ', ') AS medicos
        FROM proveedores pr
        INNER JOIN proporcionan prop ON pr.id = prop.idProveedor
        INNER JOIN productos pd ON prop.idProductos = pd.idProductos
        LEFT JOIN recetan r ON r.idProductos = pd.idProductos
        LEFT JOIN medicos m ON m.id = r.idMedico
        GROUP BY pr.nombre, pr.cif
    ";

        // Ejecutar la consulta SQL y obtener los resultados
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($consultaSql);
        $resultados = $command->queryAll();

        // Pasar los resultados a un DataProvider
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $resultados,
            'pagination' => false, // Desactivar la paginación para mostrar todos los resultados
        ]);

        // Renderizar la vista 'lista' pasando el DataProvider
        return $this->render('lista', [
                    'dataProvider' => $dataProvider,
        ]);
    }

}
