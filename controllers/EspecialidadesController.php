<?php

namespace app\controllers;

use app\models\ModeloEspecialidades;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModeloMedicos;
use app\models\Medicos;

/**
 * EspecialidadesController implements the CRUD actions for ModeloEspecialidades model.
 */
class EspecialidadesController extends Controller {

    /**
     * Lists all ModeloEspecialidades models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloEspecialidades::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloEspecialidades model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModeloEspecialidades model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
    $model = new ModeloEspecialidades();

    // Obtener los nombres completos de los médicos
    $medicos = ModeloMedicos::find()
        ->select(["CONCAT(nombre, ' ', apellidos) AS nombreCompleto"])
        ->indexBy('id')
        ->column();

    // Obtener todas las especialidades disponibles sin duplicados
    $especialidades = ModeloEspecialidades::find()
        ->select('especialidades')
        ->distinct()
        ->column();

    if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
    }

    return $this->render('create', [
        'model' => $model,
        'medicos' => $medicos,
        'especialidades' => $especialidades,
    ]);
}

    /**
     * Updates an existing ModeloEspecialidades model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        // Obtener los nombres completos de los médicos
        $medicos = ModeloMedicos::find()
                ->select(['nombre', 'apellidos', 'id'])
                ->asArray()
                ->all();

        $medicosDropDown = [];
        foreach ($medicos as $medico) {
            $nombreCompleto = $medico['nombre'] . ' ' . $medico['apellidos'];
            $medicosDropDown[$medico['id']] = $nombreCompleto;
        }

        // Obtener todas las especialidades disponibles sin duplicados
        $especialidades = ModeloEspecialidades::find()
                ->select('especialidades')
                ->distinct()
                ->indexBy('especialidades')
                ->column();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
                    'medicos' => $medicosDropDown,
                    'especialidades' => $especialidades,
        ]);
    }

    /**
     * Deletes an existing ModeloEspecialidades model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloEspecialidades model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ModeloEspecialidades the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ModeloEspecialidades::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Action to display the best specialties with the number of associated doctors.
     *
     * @return string
     */
    public function actionMejoresEspecialidades() {
        // Consultar la cantidad de médicos y pacientes asociados a cada especialidad
        $mejoresEspecialidades = ModeloEspecialidades::find()
                ->select([
                    'especialidades',
                    'COUNT(DISTINCT medicos.id) as totalMedicos',
                    'COUNT(pacientes.id) as totalPacientes'
                ])
                ->leftJoin('medicos', 'medicos.id = especialidades.idMedico')
                ->leftJoin('pacientes', 'pacientes.idMedico = medicos.id')
                ->groupBy('especialidades')
                ->orderBy(['totalMedicos' => SORT_DESC])
                ->limit(5) // Limita la consulta a las 5 especialidades con más médicos
                ->asArray()
                ->all();

        // Renderizar la vista con los datos obtenidos
        return $this->render('mejores-especialidades', [
                    'mejoresEspecialidades' => $mejoresEspecialidades,
        ]);
    }

}
