<?php

namespace app\controllers;

use Yii;
use app\models\ModeloProporcionan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModeloProductos;
use app\models\ModeloProveedores;
/**
 * ProporcionanController implements the CRUD actions for ModeloProporcionan model.
 */
class ProporcionanController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        // Agrega el filtro de verbos HTTP a los comportamientos heredados
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all ModeloProporcionan models.
     *
     * @return string
     */
    public function actionIndex() {
        // Crea un proveedor de datos para mostrar todos los registros de proporcionan
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloProporcionan::find(),
        ]);

        // Renderiza la vista 'index' con los datos proporcionados
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloProporcionan model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        // Renderiza la vista 'view' para mostrar los detalles de un registro de proporcionan específico
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModeloProporcionan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new ModeloProporcionan();

        // Obtener modelos de productos y proveedores
        $productos = ModeloProductos::find()->all();
        $proveedores = ModeloProveedores::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
                    'productos' => $productos,
                    'proveedores' => $proveedores,
        ]);
    }

    /**
     * Updates an existing ModeloProporcionan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        // Busca el modelo de proporcionan por su ID
        $model = $this->findModel($id);

        // Si se envían datos de formulario y se actualiza el modelo exitosamente
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // Redirige a la vista 'view' para mostrar los detalles del registro de proporcionan actualizado
            return $this->redirect(['view', 'id' => $model->id]);
        }

        // Renderiza la vista 'update' para mostrar el formulario de actualización de proporcionan
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ModeloProporcionan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        // Busca y elimina el registro de proporcionan por su ID
        $this->findModel($id)->delete();

        // Redirige a la vista 'index' para mostrar todos los registros de proporcionan restantes
        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloProporcionan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ModeloProporcionan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        // Busca y devuelve el modelo de proporcionan por su ID
        if (($model = ModeloProporcionan::findOne(['id' => $id])) !== null) {
            return $model;
        }

        // Si el modelo no se encuentra, lanza una excepción de página no encontrada
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
