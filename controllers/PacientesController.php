<?php

namespace app\controllers;

use app\models\ModeloPacientes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModeloMedicos;
use app\models\Pacientes;

/**
 * PacientesController implements the CRUD actions for ModeloPacientes model.
 */
class PacientesController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        // Hereda los comportamientos de la clase padre y agrega el filtro de verbos HTTP
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all ModeloPacientes models.
     *
     * @return string
     */
    public function actionIndex() {
        // Crea un proveedor de datos para mostrar todos los pacientes
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloPacientes::find(),
        ]);

        // Renderiza la vista 'index' con los datos proporcionados
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloPacientes model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        // Renderiza la vista 'view' para mostrar los detalles de un paciente específico
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModeloPacientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new ModeloPacientes();

        // Obtener la lista de médicos
        $medicos = ModeloMedicos::find()->all();
        $listaMedicos = [];
        foreach ($medicos as $medico) {
            $listaMedicos[$medico->id] = $medico->getNombreCompleto();
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
                    'listaMedicos' => $listaMedicos, // Pasar la lista de médicos a la vista
        ]);
    }

    /**
     * Updates an existing ModeloPacientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        // Busca el modelo de paciente por su ID
        $model = $this->findModel($id);

        // Obtener la lista de médicos
        $medicos = ModeloMedicos::find()->all();
        $listaMedicos = [];
        foreach ($medicos as $medico) {
            $listaMedicos[$medico->id] = $medico->getNombreCompleto();
        }

        // Ya no es necesario imprimir la variable $listaMedicos
        // var_dump($listaMedicos); // Comentario esta línea
        // Si se envían datos de formulario y se actualiza el modelo exitosamente
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // Redirige a la vista 'view' para mostrar los detalles del paciente actualizado
            return $this->redirect(['view', 'id' => $model->id]);
        }

        // Renderiza la vista 'update' para mostrar el formulario de actualización de pacientes
        return $this->render('update', [
                    'model' => $model,
                    'listaMedicos' => $listaMedicos, // Pasar la lista de médicos a la vista
        ]);
    }

    /**
     * Deletes an existing ModeloPacientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        // Busca y elimina el modelo de paciente por su ID
        $this->findModel($id)->delete();

        // Redirige a la vista 'index' para mostrar todos los pacientes restantes
        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloPacientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ModeloPacientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        // Busca y devuelve el modelo de paciente por su ID
        if (($model = ModeloPacientes::findOne(['id' => $id])) !== null) {
            return $model;
        }

        // Si el modelo no se encuentra, lanza una excepción de página no encontrada
        throw new NotFoundHttpException('La solicitud no existe');
    }

}
