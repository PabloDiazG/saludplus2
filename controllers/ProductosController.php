<?php

namespace app\controllers;

use Yii;
use app\models\ModeloProductos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModeloProporcionan;

/**
 * ProductosController implements the CRUD actions for ModeloProductos model.
 */
class ProductosController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        // Agrega el filtro de verbos HTTP a los comportamientos heredados
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all ModeloProductos models.
     *
     * @return string
     */
    public function actionIndex() {
        // Crea un proveedor de datos para mostrar todos los productos
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloProductos::find(),
        ]);

        // Renderiza la vista 'index' con los datos proporcionados
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloProductos model.
     * @param int $idProductos Id Productos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idProductos) {
        // Renderiza la vista 'view' para mostrar los detalles de un producto específico
        return $this->render('view', [
                    'model' => $this->findModel($idProductos),
        ]);
    }

    /**
     * Creates a new ModeloProductos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        // Crea un nuevo modelo de producto
        $model = new ModeloProductos();

        // Si se envían datos de formulario y se guarda el modelo exitosamente
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                // Redirige a la vista 'view' para mostrar los detalles del nuevo producto
                return $this->redirect(['view', 'idProductos' => $model->idProductos]);
            }
        } else {
            // Carga los valores predeterminados del modelo
            $model->loadDefaultValues();
        }

        // Renderiza la vista 'create' para mostrar el formulario de creación de productos
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing ModeloProductos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idProductos Id Productos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idProductos) {
        // Busca el modelo de producto por su ID
        $model = $this->findModel($idProductos);

        // Si se envían datos de formulario y se actualiza el modelo exitosamente
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // Redirige a la vista 'view' para mostrar los detalles del producto actualizado
            return $this->redirect(['view', 'idProductos' => $model->idProductos]);
        }

        // Renderiza la vista 'update' para mostrar el formulario de actualización de productos
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ModeloProductos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idProductos Id Productos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idProductos) {
        // Busca y elimina el modelo de producto por su ID
        $this->findModel($idProductos)->delete();

        // Redirige a la vista 'index' para mostrar todos los productos restantes
        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloProductos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idProductos Id Productos
     * @return ModeloProductos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idProductos) {
        // Busca y devuelve el modelo de producto por su ID
        if (($model = ModeloProductos::findOne(['idProductos' => $idProductos])) !== null) {
            return $model;
        }

        // Si el modelo no se encuentra, lanza una excepción de página no encontrada
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionProductosProporcionados() {
        // Consulta la base de datos para obtener los productos proporcionados
        $productosProporcionados = ModeloProporcionan::find()
                ->select(['productos.nombre', 'COUNT(*) AS cantidad_proveedores'])
                ->leftJoin('productos', 'productos.idProductos = proporcionan.idProductos')
                ->groupBy('productos.idProductos')
                ->orderBy(['cantidad_proveedores' => SORT_DESC])
                ->limit(5) // Limita los resultados a los 5 productos con más proveedores
                ->asArray()
                ->all();

        // Renderiza la vista 'productos_proporcionados' con los datos obtenidos
        return $this->render('productos_proporcionados', [
                    'productosProporcionados' => $productosProporcionados,
        ]);
    }

}
