<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            // Control de acceso para restringir el acceso a ciertas acciones solo a usuarios autenticados
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // Filtro de verbos HTTP para restringir las acciones a verbos específicos
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            // Acción para manejar errores, como errores 404, 500, etc.
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            // Acción para mostrar el captcha en formularios
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        // Renderiza la página de inicio
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        // Redirige al usuario si ya está autenticado
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        // Crea una instancia del modelo de formulario de inicio de sesión
        $model = new LoginForm();

        // Carga los datos del formulario y autentica al usuario si se envió el formulario
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        // Si no se envió el formulario o la autenticación falló, muestra la vista de inicio de sesión
        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        // Cierra la sesión del usuario y lo redirige a la página de inicio
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        // Crea una instancia del modelo de formulario de contacto
        $model = new ContactForm();

        // Procesa el formulario de contacto si se envió
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            // Muestra un mensaje de éxito y refresca la página
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }

        // Muestra la vista de contacto con el modelo de formulario
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        // Muestra la vista de la página "Acerca de"
        return $this->render('about');
    }

}
