<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "procedimientos".
 *
 * @property int $id
 * @property int $numExpediente
 * @property string|null $detalles
 * @property int $idPaciente
 *
 * @property Medicos[] $idMedicos
 * @property Pacientes $idPaciente0
 * @property Realizan[] $realizans
 */
class ModeloProcedimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'procedimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numExpediente', 'idPaciente'], 'required'],
            [['numExpediente', 'idPaciente'], 'integer'],
            [['detalles'], 'string', 'max' => 200],
            [['numExpediente'], 'string', 'max' => 4],
            [['numExpediente'], 'unique'],
            [['idPaciente'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPaciente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numExpediente' => 'Número de Expediente',
            'detalles' => 'Detalles',
            'idPaciente' => 'Nombre del paciente',
        ];
    }

    /**
     * Gets query for [[IdMedicos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedicos()
    {
        return $this->hasMany(Medicos::class, ['id' => 'idMedico'])->viaTable('realizan', ['idProcedimiento' => 'id']);
    }

    /**
     * Gets query for [[IdPaciente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPaciente0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPaciente']);
    }

    /**
     * Gets query for [[Realizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRealizans()
    {
        return $this->hasMany(Realizan::class, ['idProcedimiento' => 'id']);
    }
    
    public function getPaciente()
    {
        return $this->hasOne(ModeloPacientes::className(), ['id' => 'idPaciente']);
    }
}
