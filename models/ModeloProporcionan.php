<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proporcionan".
 *
 * @property int $id
 * @property int $idProveedor
 * @property int $idProductos
 *
 * @property Productos $idProductos0
 * @property Proveedores $idProveedor0
 */
class ModeloProporcionan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proporcionan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProveedor', 'idProductos'], 'required'],
            [['idProveedor', 'idProductos'], 'integer'],
            [['idProveedor', 'idProductos'], 'unique', 'targetAttribute' => ['idProveedor', 'idProductos'], 'message' => 'Este proveedor ya ha sido asignado con este producto'],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'idProductos']],
            [['idProveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::class, 'targetAttribute' => ['idProveedor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idProveedor' => 'Nombre del proveedor',
            'idProductos' => 'Nombre del producto',
        ];
    }

    /**
     * Gets query for [[IdProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['idProductos' => 'idProductos']);
    }

    /**
     * Gets query for [[IdProveedor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProveedor0()
    {
        return $this->hasOne(Proveedores::class, ['id' => 'idProveedor']);
    }
}
