<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pacientes".
 *
 * @property int $id
 * @property string $dni
 * @property string|null $direccion
 * @property string $apellido
 * @property string $nombre
 * @property string|null $expediente
 * @property int $idMedico
 * @property string|null $fechaDeConsulta
 * @property bool|null $atendido
 *
 * @property Medicos $idMedico0
 * @property Productos[] $idProductos
 * @property Procedimientos[] $procedimientos
 * @property Telefonospaciente $telefonospaciente
 * @property Utilizan[] $utilizans
 */
class ModeloPacientes extends ActiveRecord {

    public static function tableName() {
        return 'pacientes';
    }

    public function rules() {
        return [
            [['dni', 'apellido', 'nombre', 'idMedico'], 'required'],
            [['idMedico'], 'integer'],
            [['nombre'], 'match', 'pattern' => '/^(?!.*\s{2})[a-zA-ZáéíóúÁÉßÍÓÚñÑüÜÖö]+(?: [a-zA-ZáéíóúÁÉÍßÓÚñÖöÑüÜ]+)*$/', 'message' => 'El nombre no puede contener doble espacio ni números.'],
            [['apellido'], 'match', 'pattern' => '/^(?!.*\s{2})[a-zA-ZáéíóúÁÉßÍÓÚñÑüÜÖö]+(?: [a-zA-ZáéíóúÁßÉÍÓÚñÑÖöüÜ]+)*$/', 'message' => 'El apellido no puede contener doble espacio ni números.'],
            [['dni'], 'match', 'pattern' => '/^\d{8}[A-Z]$/i', 'message' => 'El DNI debe contener 8 números seguidos de una letra sin tilde. Ejemplo: 66832302Y'],
            [['dni'], 'validateDni'], // Validación personalizada
            [['atendido'], 'boolean'],
            [['dni'], 'string', 'max' => 9],
            [['direccion', 'apellido'], 'string', 'max' => 50],
            [['nombre'], 'string', 'max' => 20],
            [['expediente'], 'string', 'max' => 500],
            [['dni'], 'unique'],
            [['idMedico'], 'exist', 'skipOnError' => true, 'targetClass' => ModeloMedicos::class, 'targetAttribute' => ['idMedico' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'dni' => 'DNI',
            'direccion' => 'Dirección',
            'apellido' => 'Apellido',
            'nombre' => 'Nombre',
            'expediente' => 'Expediente',
            'idMedico' => 'Nombre del médico',
            /*             * 'fechaDeConsulta' => 'Fecha De Consulta', */
            'atendido' => 'Atendido',
        ];
    }

    public function validateDni($attribute, $params) {
        $dni = $this->$attribute;
        $number = substr($dni, 0, 8);
        $letter = strtoupper(substr($dni, -1)); // Convertir la letra a mayúscula

        if (!$this->isValidDniLetter($number, $letter)) {
            $this->addError($attribute, 'La letra del DNI no es correcta para los números proporcionados.');
        }

        // Actualizar el valor del DNI con la letra en mayúscula
        $this->$attribute = $number . $letter;
    }

    private function isValidDniLetter($number, $letter) {
        $letters = "TRWAGMYFPDXBNJZSQVHLCKE";
        return $letters[$number % 23] === $letter;
    }

    public function getIdMedico0() {
        return $this->hasOne(Medicos::class, ['id' => 'idMedico']);
    }

    public function getIdProductos() {
        return $this->hasMany(Productos::class, ['idProductos' => 'idProductos'])->viaTable('utilizan', ['idPaciente' => 'id']);
    }

    public function getProcedimientos() {
        return $this->hasMany(Procedimientos::class, ['idPaciente' => 'id']);
    }

    public function getTelefonospaciente() {
        return $this->hasOne(Telefonospaciente::class, ['idPaciente' => 'id']);
    }

    public function getUtilizans() {
        return $this->hasMany(Utilizan::class, ['idPaciente' => 'id']);
    }

    public function getNombreCompleto() {
        return $this->nombre . ' ' . $this->apellido;
    }

    public function getPaciente() {
        return $this->hasOne(ModeloPacientes::class, ['id' => 'idPaciente']);
    }

    public $listaMedicos;

}