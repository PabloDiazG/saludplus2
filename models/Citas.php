<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "medicos".
 *
 * @property int $id
 * @property string $nombre
 * @property string $direccion
 * @property string $apellidos
 * @property int $experiencia
 *
 * @property ModeloPacientes[] $pacientes
 */
class Citas extends ActiveRecord
{
    public static function tableName()
    {
        return 'medicos';
    }

    public function rules()
    {
        return [
            [['nombre', 'direccion', 'apellidos', 'experiencia'], 'required'],
            [['experiencia'], 'integer'],
            [['nombre', 'direccion', 'apellidos'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'direccion' => 'Dirección',
            'apellidos' => 'Apellidos',
            'experiencia' => 'Experiencia (años)',
        ];
    }

    public function getPacientes()
    {
        return $this->hasMany(ModeloPacientes::class, ['idMedico' => 'id'])
            ->andWhere(['or', ['atendido' => null], ['atendido' => false]]);
    }
}
