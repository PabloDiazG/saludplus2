<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property int $id
 * @property string $cif
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $ubicacion
 *
 * @property Productos[] $idProductos
 * @property Proporcionan[] $proporcionans
 */
class ModeloProveedores extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['cif', 'nombre'], 'required'],
            [['cif'], 'match', 'pattern' => '/^[A-Z]\d{7}[A-Z]$/i', 'message' => 'El CIF debe tener el formato correcto. Ejemplo: D1816329F'],
            [['nombre'], 'match', 'pattern' => '/^[a-zA-Z]+(?: [a-zA-Z]+)*$/', 'message' => 'El nombre debe contener solo letras de la A-Z y como máximo un espacio entre palabras.'],
            [['descripcion', 'ubicacion'], 'string'],
            [['nombre'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 200],
            [['ubicacion'], 'string', 'max' => 50],
            [['cif'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'cif' => 'Cif',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'ubicacion' => 'Ubicación',
        ];
    }

    /**
     * Gets query for [[IdProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos() {
        return $this->hasMany(Productos::class, ['idProductos' => 'idProductos'])->viaTable('proporcionan', ['idProveedor' => 'id']);
    }

    /**
     * Gets query for [[Proporcionans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProporcionans() {
        return $this->hasMany(Proporcionan::class, ['idProveedor' => 'id']);
    }

    public $listaProveedores;

}
