<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "realizan".
 *
 * @property int $id
 * @property int $idProcedimiento
 * @property int $idMedico
 *
 * @property Medicos $idMedico0
 * @property Procedimientos $idProcedimiento0
 */
class Realizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'realizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProcedimiento', 'idMedico'], 'required'],
            [['idProcedimiento', 'idMedico'], 'integer'],
            [['idProcedimiento', 'idMedico'], 'unique', 'targetAttribute' => ['idProcedimiento', 'idMedico']],
            [['idMedico'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['idMedico' => 'id']],
            [['idProcedimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Procedimientos::class, 'targetAttribute' => ['idProcedimiento' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idProcedimiento' => 'Id Procedimiento',
            'idMedico' => 'Id Medico',
        ];
    }

    /**
     * Gets query for [[IdMedico0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedico0()
    {
        return $this->hasOne(Medicos::class, ['id' => 'idMedico']);
    }

    /**
     * Gets query for [[IdProcedimiento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProcedimiento0()
    {
        return $this->hasOne(Procedimientos::class, ['id' => 'idProcedimiento']);
    }
}
