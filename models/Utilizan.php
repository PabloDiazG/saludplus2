<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilizan".
 *
 * @property int $id
 * @property int $idPaciente
 * @property int $idProductos
 *
 * @property Pacientes $idPaciente0
 * @property Productos $idProductos0
 */
class Utilizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'utilizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPaciente', 'idProductos'], 'required'],
            [['idPaciente', 'idProductos'], 'integer'],
            [['idPaciente', 'idProductos'], 'unique', 'targetAttribute' => ['idPaciente', 'idProductos']],
            [['idPaciente'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPaciente' => 'id']],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'idProductos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPaciente' => 'Id Paciente',
            'idProductos' => 'Id Productos',
        ];
    }

    /**
     * Gets query for [[IdPaciente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPaciente0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPaciente']);
    }

    /**
     * Gets query for [[IdProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['idProductos' => 'idProductos']);
    }
}
