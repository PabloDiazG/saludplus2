<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonosmedico".
 *
 * @property int $id
 * @property int $idMedico
 * @property string|null $telefonosMedico
 *
 * @property Medicos $idMedico0
 */
class ModeloTelefonosmedico extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'telefonosmedico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
        [['idMedico'], 'required'],
        [['idMedico'], 'integer'],
        [['telefonosMedico'], 'match', 'pattern' => '/^[67]\d{8}$/', 'message' => 'El teléfono debe comenzar con 6 o 7 y tener 9 dígitos en total.'],
        [['telefonosMedico'], 'string', 'max' => 9],
        [['idMedico'], 'unique'],
        [['idMedico'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['idMedico' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'idMedico' => 'Médico',
            'telefonosMedico' => 'Teléfonos del médico',
        ];
    }

    /**
     * Gets query for [[IdMedico0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedico0() {
        return $this->hasOne(Medicos::class, ['id' => 'idMedico']);
    }

}
