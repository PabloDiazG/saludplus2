<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "medicos".
 *
 * @property int $id
 * @property string $nombre
 * @property string|null $direccion
 * @property string $apellidos
 * @property int|null $experiencia
 *
 * @property Especialidades[] $especialidades
 * @property Procedimientos[] $idProcedimientos
 * @property Productos[] $idProductos
 * @property Pacientes[] $pacientes
 * @property Realizan[] $realizans
 * @property Recetan[] $recetans
 * @property Telefonosmedico $telefonosmedico
 */
class Medicos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medicos';
    }

    /**
     * {@inheritdoc}
     */
     public function rules() {
        return [
            [['nombre', 'apellidos'], 'required'],
            [['nombre'], 'match', 'pattern' => '/^[a-zA-Z]+(?: [a-zA-Z]+)*$/i', 'message' => 'El nombre solo puede contener letras de la A-Z y como mucho un espacio entre palabras.'],
            [['experiencia'], 'integer'],
            [['direccion'], 'match', 'pattern' => '/^[a-zA-Z0-9\s]+,\s\d{5},\s[a-zA-Z0-9\s]+$/i', 'message' => 'El formato de la dirección no es válido. Ejemplo de formato válido: Calle Ejemplo 123, 12345, Ciudad Ejemplo.'],
            [['apellidos'], 'match', 'pattern' => '/^[a-zA-Z]+(?: [a-zA-Z]+)*$/i', 'message' => 'Los apellidos solo pueden contener letras de la A-Z y como mucho un espacio entre palabras.'],
            [['apellidos', 'direccion'], 'string', 'max' => 50],
            [['nombre'], 'string', 'max' => 20],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'apellidos' => 'Apellidos',
            'experiencia' => 'Experiencia',
        ];
    }

    /**
     * Gets query for [[Especialidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEspecialidades()
    {
        return $this->hasMany(Especialidades::class, ['idMedico' => 'id']);
    }

    /**
     * Gets query for [[IdProcedimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProcedimientos()
    {
        return $this->hasMany(Procedimientos::class, ['id' => 'idProcedimiento'])->viaTable('realizan', ['idMedico' => 'id']);
    }

    /**
     * Gets query for [[IdProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos()
    {
        return $this->hasMany(Productos::class, ['idProductos' => 'idProductos'])->viaTable('recetan', ['idMedico' => 'id']);
    }

    /**
     * Gets query for [[Pacientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPacientes()
    {
        return $this->hasMany(Pacientes::class, ['idMedico' => 'id']);
    }

    /**
     * Gets query for [[Realizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRealizans()
    {
        return $this->hasMany(Realizan::class, ['idMedico' => 'id']);
    }

    /**
     * Gets query for [[Recetans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetans()
    {
        return $this->hasMany(Recetan::class, ['idMedico' => 'id']);
    }
    
    public function getNombreCompleto() {
        return $this->nombre . ' ' . $this->apellidos;
    }

    /**
     * Gets query for [[Telefonosmedico]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonosmedico()
    {
        return $this->hasOne(Telefonosmedico::class, ['idMedico' => 'id']);
    }
}
