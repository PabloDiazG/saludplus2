<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonospaciente".
 *
 * @property int $id
 * @property int $idPaciente
 * @property string|null $telefonosPaciente
 *
 * @property Pacientes $idPaciente0
 */
class Telefonospaciente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonospaciente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['idPaciente'], 'required'],
            [['idPaciente'], 'integer'],
            [['telefonosPaciente'], 'match', 'pattern' => '/^[67]\d{8}$/', 'message' => 'El teléfono debe comenzar con 6 o 7 y tener 9 dígitos en total.'],
            [['telefonosPaciente'], 'string', 'max' => 20],
            [['idPaciente'], 'unique'],
            [['idPaciente'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPaciente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPaciente' => 'Paciente',
            'telefonosPaciente' => 'Teléfonos Paciente',
        ];
    }

    /**
     * Gets query for [[IdPaciente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPaciente0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPaciente']);
    }
}
