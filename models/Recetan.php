<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recetan".
 *
 * @property int $id
 * @property int $idProductos
 * @property int $idMedico
 *
 * @property Medicos $idMedico0
 * @property Productos $idProductos0
 */
class Recetan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recetan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProductos', 'idMedico'], 'required'],
            [['idProductos', 'idMedico'], 'integer'],
            [['idProductos', 'idMedico'], 'unique', 'targetAttribute' => ['idProductos', 'idMedico']],
            [['idMedico'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['idMedico' => 'id']],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'idProductos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idProductos' => 'Id Productos',
            'idMedico' => 'Id Medico',
        ];
    }

    /**
     * Gets query for [[IdMedico0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedico0()
    {
        return $this->hasOne(Medicos::class, ['id' => 'idMedico']);
    }

    /**
     * Gets query for [[IdProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['idProductos' => 'idProductos']);
    }
}
